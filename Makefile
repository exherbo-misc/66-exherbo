# 66 policy for exherbo
PREFIX  ?= /usr/local
DATAROOTDIR ?= $(PREFIX)/share
SYSSERVICEDIR ?= $(DATAROOTDIR)/66/service
CONFIGDIR ?= /etc/

all: install

install:
	mkdir -p "$(DESTDIR)/$(CONFIGDIR)"
	cp -r --preserve=mode etc/* "$(DESTDIR)/$(CONFIGDIR)"

	mkdir -p "$(DESTDIR)/$(SYSSERVICEDIR)"
	cp -r --preserve=mode usr/share/66/service/* "$(DESTDIR)/$(SYSSERVICEDIR)"
